
//important to comment each method
//ctrl-alt-l, tidy formatting important part of refactoring.


package ictgradschool.industry.lab_refactor.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {
	//made instance fields
	public String line;
	public String output;
	public int classSize;
	public ArrayList<String> firstNameList;
	public ArrayList<String> surnameList ;

	//new Constructor
	public ExcelNew(){}

	//new get data method
	public ArrayList<String> getDataFromFile(String filename){
		ArrayList<String> someList = new ArrayList<String>();
		try{
		BufferedReader br = new BufferedReader(new FileReader(filename));

		while((line = br.readLine())!= null){
			someList.add(line);
		}
		br.close();

	}catch(IOException e){
			System.out.println(e);
		}return someList;
	}

	//put everything in start method
	public void start(){



			//get arrays using one method that includes try catch, open and shut buffered reader.
			firstNameList=getDataFromFile("FirstNames.txt");
			surnameList = getDataFromFile("Surnames.txt");

			classSize = 550;
			output="";
			//getStudentString
			for(int i = 1; i <= classSize; i++){
				String student = getStudentString(i);
				output += student;
			}

			try{
			BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
			bw.write(output);
			bw.close();
		}catch (IOException e){}

	}
//calls get LabMark for Lab and test but not exam
	private String getStudentString(int i) {
		String student = "";
		student = makeStudentID(i, student);
		student = setStudentName(student);
		//Student Skill
		int randStudentSkill = (int)(Math.random()*101);
		//Labs//////////////////////////
		int numLabs = 3;
		for(int j = 0; j < numLabs; j++){
            student = getLabMark(student, randStudentSkill, 15, 25, 65);
        }
		//Test/////////////////////////
		student = getLabMark(student, randStudentSkill, 20, 65, 90);
		///////////////Exam////////////
		if(randStudentSkill <= 7){
            int randDNSProb = (int)(Math.random()*101);
            if(randDNSProb <= 5){
                student += ""; //DNS
            }else{
                student += (int)(Math.random()*40); //[0,39]
            }
        } else if((randStudentSkill <= 20)){
                student += ((int)(Math.random()*10) + 40); //[40,49]
        } else if((randStudentSkill <= 60)){
            student += ((int)(Math.random()*20) + 50);//[50,69]
        } else if (randStudentSkill <= 90){
            student += ((int)(Math.random()*20) + 70); //[70,89]
        } else{
            student += ((int)(Math.random()*11) + 90); //[90,100]
        }
		//////////////////////////////////
		student += "\n";
		return student;
	}

	private String getLabMark(String student, int randStudentSkill, int i2, int i3, int i4) {
		if (randStudentSkill <= 5) {
			student += (int) (Math.random() * 40); //[0,39]
		} else if ((randStudentSkill > 5) && (randStudentSkill <= i2)) {
			student += ((int) (Math.random() * 10) + 40); // [40,49]
		} else if ((randStudentSkill > i2) && (randStudentSkill <= i3)) {
			student += ((int) (Math.random() * 20) + 50); // [50,69]
		} else if ((randStudentSkill > i3) && (randStudentSkill <= i4)) {
			student += ((int) (Math.random() * 20) + 70); // [70,89]
		} else {
			student += ((int) (Math.random() * 11) + 90); //[90,100]
		}
		student += "\t";
		return student;
	}

	private String setStudentName(String student) {
		int randFNIndex = (int)(Math.random()*firstNameList.size());
		int randSNIndex = (int)(Math.random()*surnameList.size());
		student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
		return student;
	}

	private String makeStudentID(int i, String student) {//change i to make more meaningful
		if(i/10 < 1){
			student += "000" + i;
		}else if (i/100 < 1){
			student += "00" + i;
		}else if (i/1000 < 1){
			student += "0"+i;
		}else{
			student += i;
		}
		return student;
	}

	public static void main(String [] args){ExcelNew e = new ExcelNew();
	e.start();}
}