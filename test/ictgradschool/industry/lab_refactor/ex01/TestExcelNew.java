package ictgradschool.industry.lab_refactor.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import static org.junit.Assert.*;

/**
 * TODO Write tests
 */
public class TestExcelNew {
    private ExcelNew testSubject;


    @Before
    public void setup() {
        testSubject = new ExcelNew();
        testSubject.start();
    }

    //@Test
    //public void testGoodCode() {
    //assertTrue(false);
    //}

    @Test
    public void testArraysHaveContents() {
        assertNotNull(testSubject.firstNameList);
        assertNotNull(testSubject.surnameList);

    }

    @Test
    public void testStudentNumbering(){
        assertTrue(testSubject.output.startsWith("0001"));
    }
    @Test
    public void testNumberOfEntries(){
        assertEquals(testSubject.classSize, testSubject.output.split("\n").length);

    }
    @Test
    public void testLengthOfEntry(){
        String entry = testSubject.output.split("\n")[0];
        assertEquals(8, entry.split("\t").length);
    }
}
